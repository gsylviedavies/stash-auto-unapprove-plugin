package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.io.SingleLineOutputHandler;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandInputHandler;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.bitbucket.scm.git.GitScm;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.atlassian.bitbucket.util.IoUtils;
import com.atlassian.bitbucket.util.Timer;
import com.atlassian.bitbucket.util.TimerUtils;
import com.atlassian.utils.process.BaseInputHandler;
import com.atlassian.utils.process.BaseOutputHandler;
import com.atlassian.utils.process.ProcessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;

public class GitRescopeAnalyzer implements RescopeAnalyzer {

    private static final Logger log = LoggerFactory.getLogger(GitRescopeAnalyzer.class);

    private final GitCommandBuilderFactory builderFactory;
    private final AutoUnapproveConfig unapproveConfig;

    public GitRescopeAnalyzer(GitCommandBuilderFactory builderFactory, AutoUnapproveConfig unapproveConfig) {
        this.builderFactory = builderFactory;
        this.unapproveConfig = unapproveConfig;
    }

    @Override
    public boolean isUpdated(@Nonnull PullRequest pullRequest, @Nonnull String previousFrom) {
        PullRequestRef toRef = pullRequest.getToRef();
        Repository repository = toRef.getRepository();

        PullRequestRef fromRef = pullRequest.getFromRef();
        String currentFrom = fromRef.getLatestCommit();
        if (previousFrom.equals(currentFrom)) {
            return false;
        }

        // For Git repositories, use "git diff a...b | git patch-id --stable" to determine whether
        // the pull request's diff has been updated in a meaningful way
        if (GitScm.ID.equals(repository.getScmId())) {
            try (Timer ignored = TimerUtils.start("Calculate patch IDs")) {
                String oldPatchId = calculatePatchId(pullRequest, previousFrom);
                if (oldPatchId == null) {
                    // If we can't generate a patch ID for the previous diff, don't bother trying
                    // to create one for the current diff.
                    // The most likely reason a patch ID can't be generated is that the commits
                    // produce an empty diff. Two empty diffs shouldn't be considered equivalent,
                    // even though they're the "same"
                    return true;
                }
                String newPatchId = calculatePatchId(pullRequest, currentFrom);

                log.debug("Old patch ID: {}; new patch ID: {}", oldPatchId, newPatchId);
                return !oldPatchId.equals(newPatchId);
            } catch (Exception e) {
                log.warn("Failed to calculate patch-ids", e);
                // Fall-through intentional
            }
        }

        // previousFrom and currentFrom don't match, and we couldn't check patch-ids, so, since the
        // from commit has changed, consider the pull request updated
        return true;
    }


    // Credit (and kudos!) for this approach to determining whether a pull request's diff has been updated
    // goes to G. Sylvie Davies. Thanks for your contribution!
    private String calculatePatchId(PullRequest pullRequest, String fromHash) {
        String toHash = pullRequest.getToRef().getLatestCommit();
        Repository toRepo = pullRequest.getToRef().getRepository();
        Repository fromRepo = pullRequest.getFromRef().getRepository();

        GitScmCommandBuilder diffBuilder = builderFactory.builder(toRepo).command("diff")
                .argument(toHash + "..." + fromHash)
                .argument("--");
        if (pullRequest.isCrossRepository()) {
            diffBuilder.alternates(Collections.singleton(fromRepo));
        }

        try (TmpFileOutputHandler outputHandler = new TmpFileOutputHandler(unapproveConfig.getTempDir());
             Timer ignored = TimerUtils.start("Calculate patch ID for " + toHash + "..." + fromHash)) {
            File diffFile = diffBuilder.build(outputHandler).call();
            if (diffFile == null) {
                // git diff ran without error, but produced no diff. This means the commits have identical
                // contents. Return null to indicate no patch ID could be generated
                return null;
            }

            // If we make it here, we _know_ we have non-empty diff output. Given diff output, we should be able
            // to get a valid patch ID
            String patchId = builderFactory.builder(toRepo).command("patch-id")
                    .argument("--stable") // Bitbucket Server 5.x requires Git 2.2+, which has --stable
                    .inputHandler(new FileInputHandler(diffFile))
                    .build(new SingleLineOutputHandler())
                    .call();
            if (StringUtils.isEmpty(patchId)) {
                // If we make it here, it means patch-id couldn't generate a patch _but did not fail_. If it
                // had failed (read: returned a non-zero exit code), an exception would have been thrown. If
                // it can't generate a patch ID, return null
                return null;
            }

            // git patch-id output looks like:
            // 4940de4d9aa8d4e4d57450adaa146fcfd104e13c 0000000000000000000000000000000000000000
            // We're only interested in the first SHA-1
            return StringUtils.split(patchId, " ", 2)[0];
        }
    }

    /**
     * Streams a specified file to stdin.
     */
    private static class FileInputHandler extends BaseInputHandler implements CommandInputHandler {

        private final File file;

        FileInputHandler(@Nonnull File file) {
            this.file = file;
        }

        @Override
        public void process(OutputStream out) {
            try (OutputStream outputStream = out) {
                IoUtils.copy(file, outputStream);
            } catch (IOException ioe) {
                throw new RuntimeException(
                        file.getAbsolutePath() + " could not be inflated and copied to stdin", ioe);
            }
        }
    }

    /**
     * Takes stdout and writes it to a temporary file in the provided directory.
     * <p>
     * getOutput() returns the temporary file.
     * <p>
     * This output handler should be used in a try-with-resources block to ensure the temporary file is deleted.
     */
    private static class TmpFileOutputHandler
            extends BaseOutputHandler
            implements CommandOutputHandler<File>, AutoCloseable {

        private final File tempDir;

        private File tempFile;
        private long bytes;

        TmpFileOutputHandler(File tempDir) {
            this.tempDir = tempDir;
        }

        @Override
        public void close() {
            if (tempFile != null && tempFile.exists() && !tempFile.delete()) {
                // Attempt a best-effort deletion when the JVM shuts down instead of throwing
                tempFile.deleteOnExit();
            }
        }

        @Override
        public File getOutput() {
            return bytes > 0 ? tempFile : null;
        }

        @Override
        public void process(InputStream in) throws ProcessException {
            try (InputStream inputStream = in) {
                tempFile = File.createTempFile("patch", ".txt", tempDir);
                bytes = IoUtils.copy(inputStream, tempFile);
            } catch (IOException e) {
                if (tempFile == null) {
                    throw new ProcessException("A temp file could not be created for writing patch output", e);
                }
                throw new ProcessException("Output could not be copied to " + tempFile.getAbsolutePath(), e);
            }
        }
    }
}
