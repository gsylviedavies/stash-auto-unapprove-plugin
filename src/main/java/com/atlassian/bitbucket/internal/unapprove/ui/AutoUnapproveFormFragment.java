package com.atlassian.bitbucket.internal.unapprove.ui;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.ui.ContextualFormFragment;
import com.atlassian.bitbucket.ui.ValidationErrors;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.atlassian.bitbucket.view.TemplateRenderingException;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import static com.atlassian.bitbucket.internal.unapprove.DefaultAutoUnapproveService.PLUGIN_KEY;

public class AutoUnapproveFormFragment implements ContextualFormFragment {

    static final String FIELD_ERRORS = "fieldErrors";
    static final String FIELD_KEY = "autoUnapprove";
    static final String FRAGMENT_TEMPLATE = "bitbucket.internal.autoUnapprove.fragment";
    static final String TEMPLATE_MODULE_KEY = PLUGIN_KEY + ":auto-unapprove-soy-templates";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final AutoUnapproveService unapproveService;

    public AutoUnapproveFormFragment(SoyTemplateRenderer soyTemplateRenderer, AutoUnapproveService unapproveService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.unapproveService = unapproveService;
    }

    @Override
    public void doError(@Nonnull Appendable appendable, @Nonnull Map<String, String[]> requestParams,
                        @Nonnull Map<String, Collection<String>> fieldErrors,
                        @Nonnull Map<String, Object> context) throws IOException {
        context.put(FIELD_KEY, isEnabled(requestParams.get(FIELD_KEY)));
        context.put(FIELD_ERRORS, fieldErrors);

        renderView(appendable, context);
    }

    @Override
    public void doView(@Nonnull Appendable appendable, @Nonnull Map<String, Object> context) throws IOException {
        Repository repository = (Repository) context.get("repository");

        context.put(FIELD_KEY, unapproveService.isEnabled(repository));

        renderView(appendable, context);
    }

    @Override
    public void execute(@Nonnull Map<String, String[]> requestParams, @Nonnull Map<String, Object> context) {
        Repository repository = (Repository) context.get("repository");

        unapproveService.setEnabled(repository, isEnabled(requestParams.get(FIELD_KEY)));
    }

    @Override
    public void validate(@Nonnull Map<String, String[]> requestParams, @Nonnull ValidationErrors errors,
                         @Nonnull Map<String, Object> context) {
        // nothing to validate
    }

    private boolean isEnabled(String[] values) {
        return values != null && values.length > 0 && Boolean.valueOf(values[0]);
    }

    private void renderView(Appendable appendable, Map<String, Object> context) {
        try {
            soyTemplateRenderer.render(appendable, TEMPLATE_MODULE_KEY, FRAGMENT_TEMPLATE, context);
        } catch (SoyException e) {
            throw new TemplateRenderingException("Failed to render " + FRAGMENT_TEMPLATE, e);
        }
    }
}
