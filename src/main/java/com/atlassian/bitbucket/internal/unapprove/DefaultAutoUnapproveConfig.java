package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.util.FileUtils;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * @since 2.2
 */
public class DefaultAutoUnapproveConfig implements AutoUnapproveConfig {

    private final File tempDir;

    public DefaultAutoUnapproveConfig(ApplicationPropertiesService propertiesService) {
        tempDir = FileUtils.mkdir(propertiesService.getTempDir(), "unapprove");
    }

    @Nonnull
    @Override
    public File getTempDir() {
        return tempDir;
    }
}
