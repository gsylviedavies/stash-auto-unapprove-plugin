package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettingsChangedEvent;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 2.2
 */
public class DefaultAutoUnapproveService implements InternalAutoUnapproveService {

    /**
     * The plugin key no longer matches the Maven {@code groupId} and {@code artifactId}, but it can't be changed
     * because the {@link PluginSettings} are associated with this key, as well as its Marketplace listing.
     */
    public static final String PLUGIN_KEY = "com.atlassian.stash.plugin.stash-auto-unapprove-plugin";

    private static final Logger log = LoggerFactory.getLogger(DefaultAutoUnapproveService.class);

    private final EventPublisher eventPublisher;
    private final PluginSettings pluginSettings;
    private final PullRequestService pullRequestService;
    private final SecurityService securityService;
    private final TransactionTemplate transactionTemplate;
    private final PermissionValidationService validationService;

    public DefaultAutoUnapproveService(EventPublisher eventPublisher, PluginSettingsFactory pluginSettingsFactory,
                                       PullRequestService pullRequestService, SecurityService securityService,
                                       TransactionTemplate transactionTemplate,
                                       PermissionValidationService validationService) {
        this.eventPublisher = eventPublisher;
        this.pullRequestService = pullRequestService;
        this.securityService = securityService;
        this.transactionTemplate = transactionTemplate;
        this.validationService = validationService;

        pluginSettings = pluginSettingsFactory.createSettingsForKey(PLUGIN_KEY);
    }

    @Override
    public void cleanup(@Nonnull Repository repository) {
        removeSetting(repository);
    }

    @Override
    public boolean isEnabled(@Nonnull PullRequest pullRequest) {
        return isEnabled(requireNonNull(pullRequest, "pullRequest").getToRef().getRepository());
    }

    @Override
    public boolean isEnabled(@Nonnull Repository repository) {
        return pluginSettings.get(keyFor(repository)) != null;
    }

    @Override
    public void setEnabled(@Nonnull Repository repository, boolean enabled) {
        validationService.validateForRepository(requireNonNull(repository, "repository"), Permission.REPO_ADMIN);

        if (updateSetting(repository, enabled)) {
            eventPublisher.publish(new AutoUnapproveSettingsChangedEvent(this, repository, enabled));
        }
    }

    @Override
    public void withdrawApprovals(@Nonnull PullRequest pullRequest) {
        try {
            transactionTemplate.execute(() -> {
                for (PullRequestParticipant reviewer : pullRequest.getReviewers()) {
                    // the setReviewerStatus method updates the status for the currently authenticated user,
                    // so use SecurityService to impersonate the reviewer we are withdrawing approval for
                    securityService.impersonating(reviewer.getUser(), "Unapproving pull-request on behalf of user")
                            .call(() -> pullRequestService.setReviewerStatus(
                                    pullRequest.getToRef().getRepository().getId(),
                                    pullRequest.getId(),
                                    PullRequestParticipantStatus.UNAPPROVED)
                            );
                }
                return null;
            });
        } catch (IllegalPullRequestStateException e) {
            log.info("[{}] is {}, which prevents withdrawing approvals", pullRequest, pullRequest.getState());
        }
    }

    static String keyFor(Repository repository) {
        return "repo." + repository.getId() + ".auto.unapprove";
    }

    private boolean removeSetting(@Nonnull Repository repository) {
        return pluginSettings.remove(keyFor(repository)) != null;
    }

    private boolean updateSetting(@Nonnull Repository repository, boolean enabled) {
        if (enabled) {
            return !"1".equals(pluginSettings.put(keyFor(repository), "1"));
        }

        return removeSetting(repository);
    }
}
