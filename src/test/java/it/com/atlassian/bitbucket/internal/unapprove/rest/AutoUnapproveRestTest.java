package it.com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.pull.PullRequestParticipantStatus;
import com.atlassian.bitbucket.test.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import it.com.atlassian.bitbucket.internal.unapprove.rest.fragment.AutoUnapproveRestFragmentTest;
import net.sf.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.util.concurrent.TimeUnit;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getBaseURL;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.fail;

/**
 * Verifies approvals are withdrawn when pull requests are updated in certain ways.
 * <p>
 * This test <i>intentionally</i> doesn't include cases where approvals are <i>not</i> withdrawn, because, since
 * nothing changes, it's impossible to verify. (How do you differentiate between the listener taking a long time
 * to run and the listener running and not changing anything, beyond waiting a "long" time?). Instead, unit tests
 * verify both cases where approvals are and are not withdrawn, and this integration test provides end-to-end
 * verification on top of that.
 */
public class AutoUnapproveRestTest extends BaseRetryingFuncTest {

    private static final String PROJECT_KEY = "AURT";
    private static final String REPOSITORY_SLUG = "unapprove";
    private static final String USER_AUTHOR = "unapprove-author";
    private static final String USER_REVIEWER = "unapprove-reviewer";

    @ClassRule
    public static final TestContext CLASS_TEST_CONTEXT = createClassTestContext();

    @BeforeClass
    public static void setupRepositoryAndUsers() throws Exception {
        CLASS_TEST_CONTEXT
                .project(PROJECT_KEY, AutoUnapproveRestTest.class.getSimpleName())
                .repository(PROJECT_KEY, REPOSITORY_SLUG, new ClassPathResource("git/pull-requests.zip"))
                .user(USER_AUTHOR)
                .user(USER_REVIEWER)
                .repositoryPermission(PROJECT_KEY, REPOSITORY_SLUG, USER_AUTHOR, Permission.REPO_WRITE)
                .repositoryPermission(PROJECT_KEY, REPOSITORY_SLUG, USER_REVIEWER, Permission.REPO_READ);

        AutoUnapproveRestFragmentTest.enableFor(PROJECT_KEY, REPOSITORY_SLUG);
    }

    @Test
    public void testUnapproveOnReopen() {
        PullRequestTestHelper pullRequest = createPullRequest(
                "refs/heads/branch_that_differ_by_one_file_trgt",
                "refs/heads/branch_that_differ_by_one_file_src");
        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);
        pullRequest.declineSafe();

        reopen(pullRequest);
        waitForUnapprove(pullRequest);
    }

    @Test
    public void testUnapproveOnRescopeChangingDiff() throws Exception {
        PullRequestTestHelper pullRequest = createPullRequest(
                "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "refs/heads/master");
        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);

        //Introduce a new file to change the pull request's effective diff
        RepositoryTestHelper.pushCommit(getBaseURL(), USER_AUTHOR, USER_AUTHOR, PROJECT_KEY, REPOSITORY_SLUG,
                "branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "Bryan Turner", "bturner@atlassian.com", "/", "other-file.txt", "Foo",
                "Rescoping pull request to trigger unapproval");
        waitForUnapprove(pullRequest);
    }

    @Test
    public void testUnapproveOnUpdateChangingTargetBranch() {
        PullRequestTestHelper pullRequest = createPullRequest(
                "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_src");
        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);

        //Retarget the pull request to a branch which references a different tip commit
        retargetToMaster(pullRequest);
        waitForUnapprove(pullRequest);
    }

    private static void reopen(PullRequestTestHelper pullRequest) {
        RestAssured.expect().statusCode(OK.getStatusCode())
                .log().ifValidationFails()
                .given()
                .auth().preemptive().basic(USER_AUTHOR, USER_AUTHOR)
                .body(PullRequestTestHelper.toJsonObject("version", 1))
                .contentType(ContentType.JSON)
                .when()
                .post(pullRequest.getReopenUrl());
    }

    private static void retargetToMaster(PullRequestTestHelper pullRequest) {
        JSONObject toRef  = new JSONObject();
        toRef.put("id", "refs/heads/master");

        //Existing reviewers must be re-sent when updating a pull request's target branch or they
        //will be removed as part of the update
        JSONObject bodyObject = PullRequestRestTestHelper.createUpdateReviewersBody(0, USER_REVIEWER);
        bodyObject.put("toRef", toRef);

        //Re-target the pull request to master
        RestAssured.expect().statusCode(OK.getStatusCode())
                .body("version", equalTo(1))
                .log().ifValidationFails()
                .given()
                .auth().preemptive().basic(USER_AUTHOR, USER_AUTHOR)
                .body(bodyObject)
                .contentType(ContentType.JSON)
                .when()
                .put(pullRequest.getUrl());
    }

    private static void waitForUnapprove(PullRequestTestHelper pullRequest) {
        long timeout = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(3L);

        while (System.currentTimeMillis() < timeout) {
            JsonPath response = pullRequest.get();

            if (response.getBoolean("reviewers[0].approved")) {
                try {
                    Thread.sleep(250L);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();

                    fail("Interrupted while waiting for approval to be withdrawn");
                }
            } else {
                return;
            }
        }

        fail("After 5 seconds, approval had not been withdrawn");
    }

    private PullRequestTestHelper createPullRequest(String fromRef, String toRef) {
        return testContext.pullRequest(
                new PullRequestTestHelper.Builder(
                        USER_AUTHOR, USER_AUTHOR, "Testing unapprovals", null,
                        PROJECT_KEY, REPOSITORY_SLUG, fromRef,
                        PROJECT_KEY, REPOSITORY_SLUG, toRef
                ).reviewers(USER_REVIEWER));
    }
}
