package it.com.atlassian.bitbucket.internal.unapprove.rest.fragment;

import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.junit.Test;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.*;
import static org.hamcrest.Matchers.equalTo;

public class AutoUnapproveRestFragmentTest extends BaseRetryingFuncTest {

    public static void enableFor(String projectKey, String repositorySlug) {
        setEnabled(projectKey, repositorySlug, true);
    }

    @Test
    public void testFragment() {
        String projectKey = getProject1();
        String repositorySlug = getProject1Repository1();

        //Auto-unapprove should start out disabled
        assertEnabled(projectKey, repositorySlug, false);

        //Enable auto-unapprove and verify that it's retained by requesting the settings after
        setEnabled(projectKey, repositorySlug, true);
        assertEnabled(projectKey, repositorySlug, true);

        //Disable auto-unapprove and verify that it's retained by requesting the settings after
        setEnabled(projectKey, repositorySlug, false);
        assertEnabled(projectKey, repositorySlug, false);
    }

    private static void assertEnabled(String projectKey, String repositorySlug, boolean enabled) {
        RestAssured.given()
                .auth().preemptive().basic(getAdminUser(), getAdminPassword())
                .expect()
                .statusCode(200)
                .body("unapproveOnUpdate", equalTo(enabled))
                .when()
                .get(buildSettingsUrl(projectKey, repositorySlug));
    }

    private static String buildSettingsUrl(String projectKey, String repositorySlug) {
        return DefaultFuncTestData.getRepositoryRestURL(projectKey, repositorySlug) + "/settings/pull-requests";
    }

    private static void setEnabled(String projectKey, String repositorySlug, boolean enabled) {
        RestAssured.given()
                .auth().preemptive().basic(getAdminUser(), getAdminPassword())
                .body(ImmutableMap.of("unapproveOnUpdate", enabled))
                .contentType(ContentType.JSON)
                .expect()
                .statusCode(200)
                .body("unapproveOnUpdate", equalTo(enabled))
                .when()
                .post(buildSettingsUrl(projectKey, repositorySlug));
    }
}
