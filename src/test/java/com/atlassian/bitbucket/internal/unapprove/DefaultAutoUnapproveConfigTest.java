package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAutoUnapproveConfigTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    private DefaultAutoUnapproveConfig config;
    @Mock
    private ApplicationPropertiesService propertiesService;

    @Before
    public void setup() {
        when(propertiesService.getTempDir()).thenReturn(folder.getRoot());

        config = new DefaultAutoUnapproveConfig(propertiesService);
    }

    @Test
    public void testGetTempDir() {
        File tempDir = config.getTempDir();
        assertTrue(tempDir.isDirectory());
        assertEquals("unapprove", tempDir.getName());
        assertEquals(new File(folder.getRoot(), "unapprove").getAbsolutePath(), tempDir.getAbsolutePath());
    }
}
